import { GetterTree } from 'vuex'
import { RootState } from '@/store/types'
import { TimelapseState } from '@/store/timelapse/types'

export const getters: GetterTree<TimelapseState, RootState> = {
}
